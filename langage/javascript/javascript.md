# JavaScript

## Les prédicats

Un prédicat renvoie une valeur true ou false.

### Liste des falsy

Les falsy sont les prédicat renvoyant false et peuvent donner lieu a des ajouts de condition suplémentaire pour palier à
certain cas.

* falsy
* 0
* ''
* null
* undefined 
* NaN (Not a Number)

Donc, dans le cas suivant le `if` affichera ok dans la console tant que `a` ne renvoie pas `false` hors il se peux
que `a` renvoie 0 dans quelle le code se situant dans le `if` ne s'activera pas.

```javascript
    const b = 4;
    const c = 4;
    const a = b - c;

    if(a) {
        console.log('ok');
    }
```


