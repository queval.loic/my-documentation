# Git

Git est un protocole qui permet d'échanger, de stocker et de mettre à jour du code sur GitHub, GitLab etc..    
Exemple de protocole connu : `http//`

## Comment ça marche ?

Lorsque l'on dépose un projet sur GitLabs, on crée un `repository`

`Fichier local <=======> cloud <=======> Fichier extérieur`

* On `push` le fichier local sur le cloud, le cloud contient donc un repository des fichiers  
* On `revert` si le push/commit du fichier extérieur ne nous plait pas  
* On `merge` si le push/commit du fichier extérieur nous plait  
* On `pull` du cloud vers le fichier local ou extérieur pour récupérer la nouvelle version  
* On `pull request` pour demander à `merge` sa `branch annexe` sur la `branch master`
* On `commit` pour découper en plusieurs partie son pull et éviter un revert sur tout les fichiers

On retrouve la majorité de ses actions dans `VCS` (Version Contrôle système)  

**Important** : Il peut y avoir conflit lorsque deux personne travaillant sur le même projet `push` en même temps.
Dans ce cas résoudre les conflits avec la seconde personne via Webstorm (possibilité de le faire avec des commandes)

Toutes ses actions se fond sur une `branch master` destinée être la version final du code et des `branch annexe` destinée elle a développer des `feature` soit du contenu destinée à `master` que l'on sépare pour éviter tout problème et assurer une sauvegarde de secours
