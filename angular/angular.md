# Angular

Angular est un framework basé sur javascript dirigée par l'équipe du projet Angular à Google et par une communauté de 
particuliers et de sociétés.

## Mise en place  

### Installation

* Installer le CLI (Command line interface) d'Angular : `npm i -g @angular/cli`  
* Vérifiez la version d'angular installer : `ng --version`  
* Créer un nouveau projet angular : `ng new project`  
  * Options d'installation 1 : Angular rooting
  * Option d'instalation 2 : Style SCSS

### Structure de base

Une fois l'installation terminer on obtiens plusieurs fichiers :

* On y trouve les fichiers index notament `index.html` contenant `<app-root>` 
la balise principale de l'aplication Angular qui contiendra d'autre balise `<app-[name]>`
étant des modules.

* Le fichier main.ts qui contient le code `platformBrowserDynamic().bootstrapModule(AppModule)` étant le gestionaire
de navigateur (Browser) suivit du module lancer au démarre de l'aplication soit le module principal.

* Le fichier environnement servant à bootstraper l'aplication et éventuelement contenir la clé de la base de données.

* Le fichier Asset contenant image, police de caractère et éventuellement du code externe (vendors).

* Le fichier App contenant l'aplication.

### Services

Un service regroupe des appelles destiner à la base de données destiner à récupérer, modifier, supprimer et créer des 
données.
   
Pour créer un service il suffit d'utiliser la commande `ng g s [name]` dans le dossier services destiner à contenir tout 
les services de l'application.

On obtiens alors la structure de base suivante :

```js
    import { Injectable } from '@angular/core';

    @Injectable({
     providedIn: 'root'
    })
    export class UserService {

    constructor() { }
}
```

## Module

Le principe d'un module est de regrouper tout le contenu qui parle d'un même thème.

### Les décorators

Les décorateurs servent à injecter automatiquement des information dans une class en spécifiant le type d'infos que l'on 
injecte.  

```js
    @NgModule({
      imports: [RouterModule.forRoot(routes)],
      exports: [RouterModule]
    })
```
      
`@NgModule` est donc un décorateur de type Modules pouvant prendre en arguments un import et un export de module.
  
Il en existe d'autre comme :
 * `selector` qui défini le nom de la balise angular `<app-myComponent>`.  
 * `templateUrl` qui lie a la page html.
 * `styleUrls` qui lie a la page de style.
 
### Routing lazy-loading

Le lazy-loading permet d'augmenter les performances en chargeant de façon progressive les différents module en fonction 
des besoins cela permet d'éviter de charger toute l'aplication à l'ouverture et donc de diviser les temps de chargement.

Il est constituer d'une feuille de route principal :
   
```js
    import {NgModule} from '@angular/core';
    import {Routes, RouterModule} from '@angular/router';
        
        
    const routes: Routes = [
      {
        path: '',
        children: [
          {
            path: '',
            loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
          },
          {
            path: 'faq',
            loadChildren: () => import('./faq/faq.module').then(m => m.FaqModule),
          },
        ]
      }
    ];
        
    @NgModule({
      imports: [RouterModule.forRoot(routes)],
      exports: [RouterModule]
    })
    export class AppRoutingModule {
    }
```
    
Celle-ci contient la majorité des routes de l'aplication, `path` étant représentatif de l'url, `children` étant
les enfants lier au module et `loadchildren` permettant de charger leur module en fonction de l'url demander.

Celui-ci renvoie alors vers une page de route secondaire :

```js
    import {NgModule} from '@angular/core';
    import {Routes, RouterModule} from '@angular/router';
    import {HomeComponent} from './home.component';
        
        
    const routes: Routes = [
      {
        path: '',
        component: HomeComponent,
      },
    ];
        
    @NgModule({
      imports: [RouterModule.forChild(routes)],
      exports: [RouterModule]
    })
    export class HomeRouting {
    }
```

Son but est de charger le component du module auquelle il appartient avec `component`et éventuellement des sous 
pages.
